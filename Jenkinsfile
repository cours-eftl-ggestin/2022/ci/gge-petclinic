node {
    def sonarOrgKey = "cours-eftl-ggestin"
    def sonarProjectKey = "${sonarOrgKey}_gge-petclinic"

    try  {

        stage("checkout") {
            checkout scm
            def pom = readMavenPom file: 'pom.xml'
            if (pom) {
                echo "Building version ${pom.version}"
            }
            sh "chmod +x ./mvnw"
        }

        stage("build") {
            sh "./mvnw package -DskipTests"
        }

        stage("quality analysis") {
            def skipTests = ''
            if ( env.SKIP_TESTS.toBoolean() ) {
                skipTests = '-DskipTests'
            }
            sh "./mvnw verify ${skipTests} org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.projectKey=${sonarProjectKey}"
        }

        stage("archive artifacts") {
            archiveArtifacts '**/target/*.jar'
            if ( !(env.SKIP_TESTS.toBoolean()) ) {
                junit allowEmptyResults: true, skipPublishingChecks: true, testResults: '**/target/surefire-reports/*.xml'
            }
        }

    } catch (Exception error) {
        // Send mail, notification
        throw error
    }
}
